import './AirportDropdown.css'

const AirportsDropdown = ({ title, airports, dropdownId, selectedAirport, setAirportFunc }) => {

    const selected = selectedAirport ? selectedAirport : ""

    const handleSelect = (e) => {
        setAirportFunc(e.target.value);
    }

    return (
        <label htmlFor={dropdownId} >{title} {' '}
            <select className="dropdown" value={selected} id={dropdownId} onChange={handleSelect} required>
            <option className="dropdown-option" value="" disabled hidden>Choose airport</option>
                {airports.map((airport) => {
                    return (
                        <option className="dropdown-option" key={airport.id} value={airport.id}>{airport.title}</option>
                    )
                })}
            </select>
        </label>
    )
}

export default AirportsDropdown;
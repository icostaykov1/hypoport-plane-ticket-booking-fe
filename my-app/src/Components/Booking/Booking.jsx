import { useContext } from "react";
import { AirportsContext } from "../../providers/AirportContext";
import './Booking.css';

const Booking = ({ booking, deleteFunc }) => {
    const { airports } = useContext(AirportsContext);

    const names = `${booking.firstName} ${booking.lastName}`
    const depAirport = airports.find((airport) => parseInt(airport.id, 10) === parseInt(booking.departureAirportId, 10));
    const destAirport = airports.find((airport) => parseInt(airport.id, 10) === parseInt(booking.arrivalAirportId, 10));
    const depDate = booking.departureDate.slice(0, 10);
    const retDate = booking.returnDate.slice(0, 10);

    const handleDelete = () => {
        deleteFunc(booking.id);
    }

    return (
        <div className="booking-outer-container">
            <div className="booking-inner-container">
                <div className="booking-content">
                    <div className="booking-header">
                        Booking <strong>{booking.id}</strong> for <strong>{names}</strong>
                    </div>
                    From <strong>{depAirport?.title} ({depAirport?.code})</strong> on <strong>{depDate}</strong>
                    {' '}
                    to <strong>{destAirport?.title} ({destAirport?.code})</strong> on <strong>{retDate}</strong>
                </div>
                <strong className="icon-delete" onClick={handleDelete}>X</strong>
            </div>
        </div>
    )
}

export default Booking;
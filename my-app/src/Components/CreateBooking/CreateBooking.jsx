import { useContext, useState } from 'react';
import { DATE_REGEX, MAX_NAME_LENGTH, MIN_NAME_LENGTH } from '../../common/constants';
import { AirportsContext } from '../../providers/AirportContext';
import { BookingsContext } from '../../providers/BookingsContext';
import { createBooking } from '../../providers/requests';
import AirportsDropdown from '../AirportsDropdown/AirportsDropdown';
import { getToday } from './create-booking-helper';
import './CreateBooking.css'

const CreateBooking = () => {
    const [formVisible, setFormVisible] = useState(false);
    const toggleFormVisible = () => {
        setFormVisible(!formVisible);
    }
    const { airports } = useContext(AirportsContext);


    const [booking, setBooking] = useState({
        firstName: "",
        lastName: "",
        depAirport: 0,
        destAirport: 0,
        depDate: "",
        retDate: "",
    });

    const { bookings, setBookings } = useContext(BookingsContext);

    /* Form Validation */
    const [firstNameError, setFirstNameError] = useState('');
    const [lastNameError, setLastNameError] = useState('');
    const [depAirportError, setDepAirportError] = useState('');
    const [destAirportError, setDestAirportError] = useState('');
    const [depDateError, setDepDateError] = useState('');
    const [retDateError, setRetDateError] = useState('');

    const [firstNameChecked, setFirstNameChecked] = useState(false);
    const [lastNameChecked, setLastNameChecked] = useState(false);
    const [depAirportChecked, setDepAirportChecked] = useState(false);
    const [destAirportChecked, setDestAirportChecked] = useState(false);
    const [depDateChecked, setDepDateChecked] = useState(false);
    const [retDateChecked, setRetDateChecked] = useState(false);

    //Validate First Name
    const validateFirstName = (name) => {
        if (name && name.length >= MIN_NAME_LENGTH &&
            name.length <= MAX_NAME_LENGTH) {
            setFirstNameError('');
            setFirstNameChecked(true);
            return true;
        }
        setFirstNameChecked(false);
        setFirstNameError('Please, enter your first name!');
        return false;
    }

    const checkFirstName = (firstName) => {
        if (!firstName || typeof firstName !== 'string') {
            setFirstNameChecked(false);
            setFirstNameError('Please, enter your first name!');
            return;
        }
        if (firstName.length < MIN_NAME_LENGTH || firstName.length > MAX_NAME_LENGTH) {
            setFirstNameChecked(false);
            setFirstNameError(`First names must be between ${MIN_NAME_LENGTH} and ${MAX_NAME_LENGTH} characters long!`);
            return;
        }
        setFirstNameChecked(true);
        setFirstNameError('');
    }

    const updateFirstName = (firstName) => {
        validateFirstName(firstName);
        setBooking({ ...booking, firstName });
    }

    //Validate Last Name
    const validateLastName = (name) => {
        if (name && name.length >= MIN_NAME_LENGTH &&
            name.length <= MAX_NAME_LENGTH) {
            setLastNameError('');
            setLastNameChecked(true);
            return true;
        }
        setLastNameChecked(false);
        setLastNameError('Please, enter your last name!');
        return false;
    }

    const checkLastName = (lastName) => {
        if (!lastName || typeof lastName !== 'string') {
            setLastNameChecked(false);
            setLastNameError('Please, enter your last name!');
            return;
        }
        if (lastName.length < MIN_NAME_LENGTH || lastName.length > MAX_NAME_LENGTH) {
            setLastNameChecked(false);
            setLastNameError(`Last names must be between ${MIN_NAME_LENGTH} and ${MAX_NAME_LENGTH} characters long!`);
            return;
        }
        setLastNameChecked(true);
        setLastNameError('');
    }

    const updateLastName = (lastName) => {
        validateLastName(lastName);
        setBooking({ ...booking, lastName });
    }

    //Validate Departure Airport
    const depAirports = booking.destAirport
        ? airports.filter((airport) => parseInt(airport.id, 10) !== parseInt(booking.destAirport, 10))
        : airports;

    const validateDepAirport = (airport) => {
        if (!airport) {
            setDepAirportError('Please, select a destination airport');
            setDepAirportChecked(false);
            return false;
        }

        if (isNaN(airport)) {
            setDepAirportError('Please, select an airport');
            setDepAirportChecked(false);
            return false;
        }
        setDepAirportError('');
        setDepAirportChecked(true);
        return true;
    }

    const handleDepAirportSelect = (airport) => {
        if (validateDepAirport(airport)) {
            setBooking({ ...booking, depAirport: parseInt(airport, 10) });
            return;
        }
    }

    //Validate Destination Airport
    const destAirports = booking.depAirport
        ? airports.filter((airport) => parseInt(airport.id, 10) !== parseInt(booking.depAirport, 10))
        : airports;

    const validateDestAirport = (airport) => {
        if (!airport) {
            setDestAirportError('Please, select a destination airport');
            setDestAirportChecked(false);
            return false;
        }

        if (airport === booking.depAirport) {
            setDestAirportError('Please, select a destination airport different than the departure airport');
            setDestAirportChecked(false);
            return false;
        }

        if (isNaN(airport)) {
            setDestAirportError('Please, select a valid destination airport');
            setDestAirportChecked(false);
            return false;
        }
        setDestAirportError('');
        setDestAirportChecked(true);
        return true;
    }

    const handleDestAirportSelect = (airport) => {
        if (validateDestAirport(airport)) {
            setDestAirportError('');
            setDestAirportChecked(true);
            setBooking({ ...booking, destAirport: parseInt(airport, 10) });
            return;
        }

        setDestAirportError('Please, select an airport');
        setDestAirportChecked(false);
    }
    //Get Today's Date
    const today = getToday();

    //Validate Depart Date
    const validateDepDate = (date) => {
        if (!date || !date.match(DATE_REGEX)) {
            setDepDateError('Please, set a valid departure date no earlier than today.');
            setDepDateChecked(false);
            return false;
        }
        if (date < today) {
            setDepDateError('Please, pick a departure date no earlier than today.');
            setDepDateChecked(false);
            return false;
        }

        setDepDateError('');
        setDepDateChecked(true);
        return true;
    }

    const handleDepDateChange = (date) => {
        if (validateDepDate(date)) {
            setBooking({ ...booking, depDate: date })
        }
    }
    //Validate Return Date
    const validateRetDate = (date) => {
        if (!booking.depDate) {
            setRetDateError('Please, set a valid departure date before choosing a return date.');
            setRetDateChecked(false);
            return false;
        }
        if (!date || !date.match(DATE_REGEX)) {
            setRetDateError('Please, set a valid return date.');
            setRetDateChecked(false);
            return false;
        }
        if (date < booking.depDate) {
            setRetDateError('Please, pick a return date no earlier than the departure date.');
            setRetDateChecked(false);
            return false;
        }

        setRetDateError('');
        setRetDateChecked(true);
        return true;
    }

    const handleRetDateChange = (date) => {
        if (validateRetDate(date)) {
            setBooking({ ...booking, retDate: date })
        }
    }

    const validateAll = () => {
        validateFirstName(booking.firstName);
        validateLastName(booking.lastName);
        validateDepAirport(booking.depAirport);
        validateDestAirport(booking.destAirport);
        validateDepDate(booking.depDate);
        validateRetDate(booking.retDate);
    }
    /* End of Form Validation */

    /* Enable/Disable Submit Button */
    const validBooking = [firstNameChecked, lastNameChecked,
        depAirportChecked, destAirportChecked, depDateChecked, retDateChecked].every(e => e);

    /* Show errors with info button */
    const showErrors = () => {
        validateAll();

        const errorInfo = ['List of Errors:', firstNameError, lastNameError, depAirportError, destAirportError, depDateError, retDateError]
            .filter((e) => e !== '').join('\n');
        console.log(errorInfo);

        alert(errorInfo === 'List of Errors:' ? 'Please, try again.' : errorInfo);
    }

    /* Create Booking */
    const handleCreateBooking = async (e) => {
        e.preventDefault();
        const result = await createBooking(booking);
        if (result && !result.error) {
            setBookings([...bookings, result]);
        }
    }

    return (
        <div className="create-booking-form-container">
            <h2 className="create-booking-form-header" onClick={toggleFormVisible}>
                Create new booking {formVisible ? <span className="accordion-arrow">&#x25B2;</span> : <span className="accordion-arrow">&#x25BC;</span>}
            </h2>
            <form className={`create-booking-form ${formVisible ? `active` : ``}`}>
                <div className="create-booking-input-container">
                    <div className="create-booking-input-inner-container">
                        <label>
                            First Name:
                            <input
                                type="text"
                                onChange={(e) => updateFirstName(e.target.value)}
                                onBlur={(e) => checkFirstName(e.target.value)}
                                required
                            />
                        </label>
                    </div>
                </div>
                <div className="create-booking-input-container">
                    <div className="create-booking-input-inner-container">
                        <label>
                            Last Name:
                            <input
                                type="text"
                                onChange={(e) => updateLastName(e.target.value)}
                                onBlur={(e) => checkLastName(e.target.value)}
                                required
                            />
                        </label>
                    </div>
                </div>
                <div className="create-booking-input-container">
                    <div className="create-booking-input-inner-container">
                        <AirportsDropdown
                            dropdownId="departure-airport"
                            title="Departure Airport"
                            airports={depAirports}
                            selectedAirport={booking.depAirport}
                            setAirportFunc={handleDepAirportSelect}
                        />
                    </div>
                </div>
                <div className="create-booking-input-container">
                    <div className="create-booking-input-inner-container">
                        <AirportsDropdown
                            dropdownId="departure-airport"
                            title="Destination Airport"
                            airports={destAirports}
                            selectedAirport={booking.destAirport}
                            setAirportFunc={handleDestAirportSelect}
                        />
                    </div>
                </div>
                <div className="create-booking-input-container">
                    <div className="create-booking-input-inner-container">
                        <label>
                            Departure Date:
                            <input
                                type="date"
                                min={today}
                                value={booking.depDate}
                                onChange={(e) => handleDepDateChange(e.target.value)}
                                required />
                        </label>
                    </div>
                </div>
                <div className="create-booking-input-container">
                    <div className="create-booking-input-inner-container">
                        <label>
                            Date of Return:
                            <input
                                type="date"
                                disabled={!(!!booking.depDate)}
                                min={booking.depDate}
                                value={booking.retDate}
                                onChange={(e) => handleRetDateChange(e.target.value)}
                                required />
                        </label>
                    </div>
                </div>
                <div className="create-booking-form-button-container">
                    <button disabled={!validBooking} onClick={(e) => handleCreateBooking(e)}>
                        Create booking
                    </button>
                    {!validBooking && <div className="icon-info-container">
                        <span className="icon-info" onClick={showErrors}>
                            &#128712;
                        </span>
                    </div>}
                </div>

            </form>
        </div>
    )
}

export default CreateBooking;
import { useContext, useState } from "react";
import { DEFAULT_PAGE_SIZE } from "../../common/constants";
import { BookingsContext } from "../../providers/BookingsContext";
import { deleteBooking } from "../../providers/requests";
import Booking from "../Booking/Booking";
import './ListBookings.css';

const ListBookings = () => {
    const [listVisible, setListVisible] = useState(false);

    const toggleListVisible = () => {
        setListVisible(!listVisible);
    }

    const { bookings, setBookings, pageIndex, setPageIndex, totalBookings } = useContext(BookingsContext);
    const deleteBookingFunc = async (id) => {
        const result = await deleteBooking(id);
        if (result) {
            setBookings((current) => current.filter((booking) => booking.id !== id));
        }
    }

    const displayedBookings = bookings.length
        ? bookings.map((booking) => <Booking key={booking?.id} booking={booking} deleteFunc={deleteBookingFunc} />)
        : <h5>There are no bookings yet.</h5>

    const handleScroll = (e) => {
        const bottom = e.target.scrollHeight - e.target.scrollTop === e.target.clientHeight;
        if (bottom) {
            if ((pageIndex + 1) * DEFAULT_PAGE_SIZE <= totalBookings) {
                setPageIndex(pageIndex + 1);
            }
        }
    }

    return (
        <div className="bookings-outer-container">
            <h2 className="bookings-header" onClick={toggleListVisible}>
                Available Bookings {listVisible 
                ? <span className="accordion-arrow">&#x25B2;</span> 
                : <span className="accordion-arrow">&#x25BC;</span> }
                </h2>
            <div className={`bookings-inner-container ${listVisible ? `active` : ``}`} onScroll={(e) => handleScroll(e)}>
                {displayedBookings}
            </div>
        </div>
    )
}

export default ListBookings;
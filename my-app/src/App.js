import { useEffect, useState } from 'react';
import CreateBooking from './Components/CreateBooking/CreateBooking';
import ListBookings from './Components/ListBookings/ListBookings';
import { getAirports, getBookings } from './providers/requests';
import './App.css';
import { AirportsContext } from './providers/AirportContext';
import { BookingsContext } from './providers/BookingsContext';
function App() {
  const [airports, setAirports] = useState([]);
  const [bookings, setBookings] = useState([]);
  const [pageIndex, setPageIndex] = useState(0);
  const [totalBookings, setTotalBookings] = useState(0);
  useEffect(() => {
    (async () => {
      const fetchedAirports = await getAirports();
      setAirports(fetchedAirports);
    })();
  }, []);

  useEffect(() => {
    (async () => {
      const fetchedBookings = await getBookings(pageIndex);
      setBookings((current) => [...current, ...fetchedBookings.list]);
      setTotalBookings(parseInt(fetchedBookings.totalCount, 10));
    })();
  }, [pageIndex]);


  return (
    <div className="App">
      <AirportsContext.Provider value={{ airports }}>
        <BookingsContext.Provider value={{bookings, setBookings, pageIndex, setPageIndex, totalBookings}}>
          <CreateBooking />
          <ListBookings />
        </BookingsContext.Provider>
      </AirportsContext.Provider>
    </div>
  );
}

export default App;

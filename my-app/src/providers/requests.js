import { BASE_URL, DEFAULT_PAGE_INDEX, DEFAULT_PAGE_SIZE, TOKEN } from "../common/constants";

export const createBooking = async (booking) => {
    const {
        firstName,
        lastName,
        depAirport: departureAirportId,
        destAirport: arrivalAirportId,
        depDate: departureDate,
        retDate: returnDate,
    } = booking;

    const result = await fetch(`${BASE_URL}/bookings/create/?authToken=${TOKEN}`, {
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            firstName,
            lastName,
            departureAirportId: parseInt(departureAirportId, 10),
            arrivalAirportId: parseInt(arrivalAirportId, 10),
            departureDate,
            returnDate,
        }),
        method: "POST",
    });

    return await result.json();
}

export const getAirports = async () => {
    const result = await fetch(`${BASE_URL}/airports/`);
    return await result.json();
}

export const getBookings = async (pageIndex = DEFAULT_PAGE_INDEX, pageSize = DEFAULT_PAGE_SIZE) => {
    const result = await fetch(`${BASE_URL}/bookings/?authToken=${TOKEN}&pageIndex=${pageIndex}&pageSize=${pageSize}`);
    return await result.json();
}

export const getBooking = async (bookingId) => {
    const result = await fetch(`${BASE_URL}/bookings/${bookingId}/?authToken=${TOKEN}`);
    return await result.json();
}

export const deleteBooking = async (bookingId) => {
    const result = await fetch(`${BASE_URL}/bookings/delete/${bookingId}/?authToken=${TOKEN}`, {
        method: 'DELETE',
        headers: {
            "Content-Type": "application/json",
        },
    });
    if (result.status === 200) {
        return true;
    }
    return false;
}
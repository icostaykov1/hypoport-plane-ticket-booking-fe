import { createContext } from 'react';

export const BookingsContext = createContext({
    bookings: [],
    setBookings: () => {},
    pageIndex: 0,
    setPageIndex: () => {},
    totalBookings: 0,
});
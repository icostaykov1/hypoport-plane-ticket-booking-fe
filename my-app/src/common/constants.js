export const BASE_URL = 'https://vm-fe-interview-task.herokuapp.com/api'

export const TOKEN = 'OMrw4IQxGYYzGgBb9bWd8EQGs6JiBp';

export const MIN_NAME_LENGTH = 3;
export const MAX_NAME_LENGTH = 50;

export const DATE_REGEX = /^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$/;

export const DEFAULT_PAGE_INDEX = 0;
export const DEFAULT_PAGE_SIZE = 5;